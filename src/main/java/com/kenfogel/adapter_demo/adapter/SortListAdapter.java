package com.kenfogel.adapter_demo.adapter;

import com.kenfogel.adapter_demo.listsorter.ListSorter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Convert the array to a list, 
 * @author Ken
 */
public class SortListAdapter implements Sorter {

    @Override
    public int[] sortArray(int[] numbers) {

        List<Integer> intList = new ArrayList<>();
        for (int num : numbers) {
            intList.add(num);
        }

        // The class we are adapting
        ListSorter ls = new ListSorter();
        List<Integer> sortList = ls.listSort(intList);
        
        int[] array = sortList.stream().mapToInt(i->i).toArray();

        //int[] array = new int[sortList.size()];
        //for(int x = 0; x < array.length; ++x) {
        //    array[x] = sortList.get(x);
        //}
        
        return array;
    }
}
