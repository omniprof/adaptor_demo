package com.kenfogel.adapter_demo.adapter;

/**
 *
 * @author Ken
 */
public interface Sorter {
    
    public int[] sortArray(int[] numbers);
    
}
