package com.kenfogel.adapter_demo;

import com.kenfogel.adapter_demo.client.ClientNeedsSorting;

/**
 * Let's play with the adapter
 *
 */
public class App {
    
    public void perform() {
        ClientNeedsSorting cns = new ClientNeedsSorting();
        int[] sortedNumbers = cns.pleaseSortMe();
        for (int num : sortedNumbers) {
            System.out.println(" " + num);
        }       
    }

    public static void main(String[] args) {
        App app = new App();
        app.perform();
        System.exit(0);
    }
}
