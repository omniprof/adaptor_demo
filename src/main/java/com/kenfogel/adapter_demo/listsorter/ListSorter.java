package com.kenfogel.adapter_demo.listsorter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Here is a class that contains a method to sort lists of Integer objects
 * for the purpose of this demo
 * @author Ken
 */
public class ListSorter {
    
    public List<Integer> listSort(List<Integer> numbers) {
        
        ArrayList<Integer> sorted = new ArrayList<>(numbers);
        Collections.sort(sorted);
        return sorted;
    }
}
