package com.kenfogel.adapter_demo.client;

import com.kenfogel.adapter_demo.adapter.SortListAdapter;
import com.kenfogel.adapter_demo.adapter.Sorter;

/**
 * This is a client that only supply arrays of primitive ints. We want to use a
 * class that sorts Lists of Integer objects. The adapter allows us to do this.
 *
 * @author Ken
 */
public class ClientNeedsSorting {

    public int[] pleaseSortMe() {
        int[] numbers = new int[]{23, 9, 45, 2, 7, 32};

        Sorter sort = new SortListAdapter();
        return (sort.sortArray(numbers));
    }
}
