You must first clone
https://gitlab.com/omniprof/desktop_projects_dependencies.git
The nbactions.xml file for NetBeans will install this project in your local repository where all other desktop projects in your course can find it.
If you are not using NetBeans then it must be run with Maven with the goal install:install
